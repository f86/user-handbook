// COMBAK: this whole page, on alpine-conf
= `setup-alpine`

Now that you are booted in, you can log-in without a password as the root user.
Alpine has a script called `setup-alpine`, which performs basic installation procedures.
This section of the handbook describes its operation modes, as well as how to use it.
However, no script is perfect, so it is recommended that if your use-case is not so simple, or you run into issues using it, to proceed to the xref:manual.adoc[next section] which covers manual setup.
For example, if you have no access to a wired network, you will need to configure xref:manual.adoc#_networking[Networking] yourself.

== `setup-alpine -q`
The q, or "quick" mode of the `setup-alpine` script allows one to get a trivial system configuration running quickly, to then proceed with other installation tasks, possibly remotely.

Under quick mode, `setup-alpine` will:

- Setup your xref:manual.adoc#_keyboard_layout[Keyboard Layout]
- Initialize your xref:manual.adoc#_hostname[Hostname] to the default "alpine" value
- Set your primary (autodetected) interface to use xref:manual.adoc#_dhcp[DHCP], and start networking
- Enable various relevant system services, and start them
- Generate a valid `/etc/hosts` file
- Enable the default xref:manual.adoc#_repositories[repository]

[WARNING]
====
`setup-alpine`'s networking set-up will *not* work by itself for wifi.
If you have no access to an ethernet port and cable, refer to the section on xref:manual.adoc#_networking[configuring networking manually].
====

Quick mode is useful to get a system online and operational very quickly (thus the name) to then go on to use it manually.

The steps quick mode skips are, in order:

- Setting a xref:manual.adoc#_root_password[root password]
- Setting the xref:manual.adoc#_timezone[timezone]
- Setting up an optional proxy
- Enabling xref:manual.adoc#_ssh[SSH]
- Enabling xref:manual.adoc#_ntp[NTP] synchronization
- Configuring the xref:manual.adoc#_setup_disk[disk], if any
- Configuring lbu
- Configuring apk's caching mechanism

== Full `setup-alpine`
`setup-alpine` in normal mode will do all of the abovementioned steps, under some conditions, and will rely much less on default values.

It is recommended to use this mode directly if your scenario is trivial.
A trivial scenario is defined as such:

- You have a single (currently) physical disk and you want to install to it
- Your primary network connection is wired, and it is DHCP-compatible
- You do not significantly care about what bootloader or filesystem you use

If all of these apply, you can simply run `setup-alpine` and follow the prompts to set up your system.
If they do not, it may still be appropriate, but you will need to customize its behavior with an answer file and/or environment variables.

== Answer Files
You can preprogram various answers to `setup-alpine` 's questions.
`setup-alpine -f ANSWER_FILE` will use settings from the file `ANSWER_FILE` instead of asking questions interactively.
`setup-alpine -c ANSWER_FILE` will create a new answer file with the following default values.
You can edit this file and use it with `setup-alpine -f`.

// EDITOR NOTE: this include will only show up in Antora
[source,sh]
----
include::example$setup_example.sh[]
----

You can also add extra environment variables to configure the behavior of any given utility.
// TODO: link developer handbook section on setup-alpine
For more details on those, see the xref:manual.adoc[next section] of the user handbook, or the developer handbook on the technical details of those utilities.
